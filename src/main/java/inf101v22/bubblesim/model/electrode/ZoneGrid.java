package inf101v22.bubblesim.model.electrode;

import java.util.List;

import inf101v22.bubblesim.model.fluids.IFluidSpheroid;
import inf101v22.grid.CoordinateItem;
import inf101v22.grid.Grid;
import inf101v22.grid.IGrid;

public class ZoneGrid {

	IGrid<CollisionChecker> grid;
	List<IFluidSpheroid> unasigned;

	public ZoneGrid(int rows, int cols) {

		IGrid<CollisionChecker> zones = new Grid<CollisionChecker>(rows, cols);
		for (CoordinateItem<CollisionChecker> checker : zones) {
			zones.set(checker.coordinate, new CollisionChecker());
		}
	}

}
