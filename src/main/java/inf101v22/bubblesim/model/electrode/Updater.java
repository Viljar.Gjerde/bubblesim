package inf101v22.bubblesim.model.electrode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.danilopianini.util.FlexibleQuadTree;

import inf101v22.bubblesim.model.fluids.FluidSpheroid;
import inf101v22.bubblesim.model.fluids.IFluidSpheroid;
import inf101v22.grid.Pair;
import inf101v22.vector.PosVector;

public abstract class Updater implements IElectrodeUpdater {

	/** The electrode. */
	protected IElectrode electrode;
	/** The number of spheroids produced per frame. */
	private int spheroidsPerFrame = 5000;
	/** The standard new mass. */
	private double standardNewMass = 0.000000000000001;
	/** The generator. */
	private Random generator = new Random();
	/** The finished. */
	private boolean finished = false;
	/** The qt. */
	protected final FlexibleQuadTree<IFluidSpheroid> qt;
	protected Set<Pair<IFluidSpheroid>> collisions;
	protected Collection<Pair<PosVector>> moved;
	protected final int checksPerFrame = 10;

	public Updater(IElectrode electrode) {
		super();
		this.electrode = electrode;
		this.qt = new FlexibleQuadTree<>();
		Set<Pair<IFluidSpheroid>> collisions = new HashSet<Pair<IFluidSpheroid>>();
		this.collisions = Collections.synchronizedSet(collisions);
		this.moved = Collections.synchronizedCollection(new ArrayList<>());
	}

	@Override
	public Double[] getCoveredFraction() {
		class Counter extends Thread {
			int covered = 0;
			int coveredAtSurface = 0;
			int checks;
			double r;

			public Counter(int checks, double r) {
				this.checks = checks;
				this.r = r;
			}

			@Override
			public void run() {
				for (int i = 0; i < checks; i++) {
					double x = generator.nextDouble() * electrode.getSize().x;
					double y = generator.nextDouble() * electrode.getSize().y;
					PosVector position = new PosVector(x, y);
					List<IFluidSpheroid> nearbySpheroids = qt.query(x - r, y - r, x + r, y + r);
					for (IFluidSpheroid fs : nearbySpheroids) {
						PosVector bubblepos = new PosVector(fs.getPosition().x, fs.getPosition().y); // remove z value
						double distance = bubblepos.dist(position);
						if (distance < fs.getRadius()) {
							covered++;
							if (distance < fs.getContactRadius()) {
								coveredAtSurface++;
							}
							break;
						}
					}
				}
			}

			public int getCovered() {
				return covered;
			}

			public int getCoveredAtSurface() {
				return coveredAtSurface;
			}
		}
		int totalChecks = 1000000;
		int covered = 0;
		int coveredAtSurface = 0;
		double r = 0;
		// Get biggest radius
		for (IFluidSpheroid fs : electrode.getFluidSpheroids()) {
			if (fs.getRadius() > r)
				r = fs.getRadius();
		}

		int processors = Runtime.getRuntime().availableProcessors() - 2;
		if (processors < 1)
			processors = 1;
		Counter[] threads = new Counter[processors];
		for (int i = 0; i < processors; i++) {
			Counter counter = new Counter(totalChecks / processors, r);
			threads[i] = counter;
			counter.start();
		}

		for (int i = 0; i < processors; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			covered += threads[i].getCovered();
			coveredAtSurface += threads[i].getCoveredAtSurface();
		}

		return new Double[] { (double) (covered) / (double) (totalChecks),
				(double) (coveredAtSurface) / (totalChecks) };

	}

	/**
	 * Produce spheroids at random positions. Their size is normal distributed
	 * around "standardMass".
	 */
	private void produceSphereoids() {
		PosVector size = electrode.getSize();
		for (int i = 0; i < spheroidsPerFrame; i++) {
			PosVector pos = new PosVector(Math.random() * size.x, Math.random() * size.y);
			IFluidSpheroid fs = new FluidSpheroid(pos, electrode.getProducedFluid(), getNewMass(), electrode);
			pos.z = fs.getRadius() * 0.99;
			fs.setPosition(pos);
			// if (!checkNewSpheroid(fs)) { // Didnt collide with any spheroids.
			qt.insert(fs, fs.getPosition().x, fs.getPosition().y);
			electrode.addSpheroid(fs);
			// }
		}

	}

	/**
	 * Gets new normally distributed mass.
	 *
	 * @return the new mass
	 */
	private double getNewMass() {
		return Math.abs(generator.nextGaussian() * standardNewMass);
	}

	@Override
	public void startUpdater() {
		new Thread(this::update).start();
	}

	private void moveInTree(Pair<PosVector> posPair) {
		PosVector oldPos = posPair.a;
		PosVector newPos = posPair.b;
		IFluidSpheroid fs = qt.query(new double[] { oldPos.x - 1e-12, oldPos.y - 1e-12 },
				new double[] { oldPos.x + 1e-12, oldPos.y + 1e-12 }).get(0);
		qt.move(fs, new double[] { oldPos.x, oldPos.y }, new double[] { newPos.x, newPos.y });
	}

	abstract void moveSpheroid(IFluidSpheroid fs);

	/**
	 * Updates all spheroids in the electrode. After updating, waits until finished
	 * is set to false by controller to make sure all updates are recorded.
	 */
	private void update() {
		while (true) {

			if (!electrode.getPaused() && !finished) {
				List<IFluidSpheroid> spheroids = electrode.getFluidSpheroids();
//				System.out.println(spheroids.size() + " and area covered is " + getCoveredFraction());
				for (int i = 0; i < checksPerFrame; i++) {
					// Move spheroids using as many threads as are available
					this.moved.clear();
					spheroids.stream().parallel().forEach(this::moveSpheroid);
					for (Pair<PosVector> p : moved) {
						moveInTree(p);
					}
					combineSphereoids();
				}
				removeSphereoids();
				produceSphereoids();
				finished = true;
			} else {
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Combine sphereoids.
	 */
	private void combineSphereoids() {
		collisions.clear();
		List<IFluidSpheroid> allSpheroids = electrode.getFluidSpheroids();
		allSpheroids.stream().parallel().forEach(this::checkSpheroid);
		HashSet<IFluidSpheroid> removed = new HashSet<>();
		for (Pair<IFluidSpheroid> p : collisions) {
			if (!removed.contains(p.a) && !removed.contains(p.b)) {
				PosVector prePos = p.a.getPosition();
				p.a.combine(p.b);
				allSpheroids.remove(p.b);
				qt.remove(p.b, p.b.getPosition().x, p.b.getPosition().y);
				qt.move(p.a, new double[] { prePos.x, prePos.y },
						new double[] { p.a.getPosition().x, p.a.getPosition().y });
				removed.add(p.b);

			}
		}
	}

	private void removeSphereoids() {
		List<IFluidSpheroid> fluidSpheroids = electrode.getFluidSpheroids();
		PosVector size = electrode.getSize();
		// Remove spheroids if sufficiently outside frame
		for (int i = 0; i < fluidSpheroids.size(); i++) {
			IFluidSpheroid fs = fluidSpheroids.get(i);
			// if (!fluidSpheroids.get(i).insideFrame(new PosVector(0, 0),
			// PosVector.mult(size, 1.1))) {
			if (!fs.insideFrame(new PosVector(0, 0), size) || fs.getPosition().z - fs.getRadius() > size.z) {
				fluidSpheroids.remove(i);
				qt.remove(fs, fs.getPosition().x, fs.getPosition().y);
				i--;
			}
		}
	}

	/**
	 * Check spheroid.
	 *
	 * @param fs         the fs
	 * @param collisions the collisions
	 */
	private void checkSpheroid(IFluidSpheroid fs) {
		// Only check spheroids that have moved - check if worth the check also check if
		// worth implementing isZero in posvector
		// if (fs.getVelocity().mag() == 0) {
		// return;
		// }
		double x = fs.getPosition().x;
		double y = fs.getPosition().y;
		double r = fs.getRadius();
		List<IFluidSpheroid> nearbySpheroids = qt.query(x - 2 * r, y - 2 * r, x + 2 * r, y + 2 * r);
		for (IFluidSpheroid other : nearbySpheroids) {
			if (fs == other)
				continue;
			if (fs.collides(other)) {
				collisions.add(new Pair<IFluidSpheroid>(fs, other));
				break;
			}
		}
	}

	@Override
	public boolean getFinished() {
		return finished || electrode.getPaused();
	}

	@Override
	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	@Override
	public void setProucedPerFrame(int newRate) {
		spheroidsPerFrame = newRate;
	}

	@Override
	public int getProducedPerFrame() {
		return spheroidsPerFrame;
	}

}