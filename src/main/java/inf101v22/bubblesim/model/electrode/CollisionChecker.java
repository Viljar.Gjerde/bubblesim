package inf101v22.bubblesim.model.electrode;

import java.util.ArrayList;
import java.util.List;

import inf101v22.bubblesim.model.fluids.IFluidSpheroid;

public class CollisionChecker extends Thread {

	List<IFluidSpheroid> allSpheres;
	List<IFluidSpheroid> toCheck;
	public boolean active = true;
//	double x;
//	double y;
//	double width;
//	double height;

	public CollisionChecker() {
//		this.x = x;
//		this.y = y;
//		this.width = width;
//		this.height = height;
		allSpheres = new ArrayList<>();
		toCheck = new ArrayList<>();
	}

	public void combineChecker(CollisionChecker toAdd) {
//		this.x = Math.min(this.x, toAdd.x);
//		this.y = Math.min(this.y, toAdd.y);
//		this.width = this.width + toAdd.width;
//		this.height = this.height + toAdd.height;
		addCheckedSpheres(toAdd.allSpheres);
		addToCheckSpheres(toAdd.toCheck);
		toAdd.active = false;
	}

	void addCheckedSphereoid(IFluidSpheroid spheroid) {
		allSpheres.add(spheroid);
	}

	public void addCheckedSpheres(List<IFluidSpheroid> newSpheres) {
		allSpheres.addAll(newSpheres);
	}

	public void addToCheckSpheres(List<IFluidSpheroid> newToCheck) {
		toCheck.addAll(newToCheck);
	}

	public void addToCheckSphere(IFluidSpheroid toCheck) {
		this.toCheck.add(toCheck);
	}

	public List<IFluidSpheroid> getSpheres() {
		return allSpheres;
	}

	@Override
	public void run() {
		for (int i = 0; i < toCheck.size(); i++) {
			IFluidSpheroid b = toCheck.get(i);
			for (int j = 0; j < allSpheres.size(); j++) {
				IFluidSpheroid a = allSpheres.get(j);
				if (a == b)
					continue;
				if (a.collides(b)) {
					a.combine(b);
					allSpheres.remove(b);
					break;
				}
			}
			toCheck.remove(b);
			i--;
		}
	}

//	@Override
//	public void run() {
//		for (int i = 0; i < toCheck.size(); i++) {
//			IFluidSpheroid b = toCheck.get(i);
//			for (int j = 0; j < checkedSpheres.size(); j++) {
//				IFluidSpheroid a = checkedSpheres.get(j);
//				if (a == b)
//					continue;
//				if (a.collides(b)) {
//					a.combine(b);
//					toCheck.remove(b);
//					i--;
//					break;
//				}
//			}
//			checkedSpheres.add(b);
//			toCheck.remove(b);
//			i--;
//		}
//	}
//	@Override
//	public void run() {
//		for (int i = 0; i < toCheck.size(); i++) {
//			IFluidSpheroid b = toCheck.get(i);
//			for (int j = 0; j < checkedSpheres.size(); j++) {
//				IFluidSpheroid a = checkedSpheres.get(j);
//				if (a == b)
//					continue;
//				if (a.collides(b)) {
//					a.combine(b);
//					toCheck.remove(b);
//					j--;
//				}
//			}
//			checkedSpheres.add(b);
//		}
//	}
}
